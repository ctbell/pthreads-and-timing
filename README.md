### pThreads and Timing ###

An example project that demonstrates the fundamentals of creating pThreads along with taking timing measurements.

To compile and run from command line:
```
g++ pThreads_Timing.cpp CStopWatch.cpp
./a.out
```
or
```
   cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ${PWD}:/tmp -w /tmp/Default rgreen13/alpine-bash-bpp ./pThreads_Timing
```

## Reflection ##
     a) What was the major question addressed in the classroom?
     
     The main topic in class was POSIX Threads and how to use them, in addition to the concepts of running multiple trials when analyzing parallel code using multiple threads.
     Additional topics addressed were the usage of Docker, multiple libraries that can be used to take advantage of stopwatch functions and pthreads themselves, and other minor things.
     
     b) Summarize what you learned during this week of class.
     
     I learned a lot about the many elements of coding that come with pthreads. For instance, the usage of `void*` and the overall importance of pointers that make parallel programming work.
     It would appear that `void*` and type casting are the heart and soul of dealing with pthreads, besides the actual objects in the `pthread` library.
     The usage of `void*` indicates typelessness to a function's return value or a variable. This "typelessness" is actually considered to be a universal type, which can be cast to a different type fairly easily.
     Additionally, I also learned how to work with Docker, as well as some other intricacies in coding that I had not really experienced before, such as `**` notation, which indicates an address of a pointer.
     
     c) What was the most difficult aspect of what you learned in class?
     
     One of, if not the most difficult things that I had to wrap my head around was adapting to using `void*` and long instead of int, as well as type casting. There seems to be a lot of back and forth
     when it comes to casting types between `void*` and types like long or int, which is a new concept to me and was confusing at first. However, I eventually acquired a taste for it, and I believe
     that I understand these concepts now moving forward.
     
     d) Analyze the results of your code, commenting on speedup, efficiency, and scalability.
     
     My code at the start was quite flawed and was missing a lot of key elements that would allow it to function effectively as a parallel program. However, after reviewing Dr. Green's version of the solution and his template code,
     I better understood the approach. As far as observing speedup is concerned, the speedup greatly increased as more threads were used. Expectedly, the overall time slowed down as problem size increased.
     In addition, efficiency was reduced as the problem size and number of threads increased as well. This is because not all resources are being utilized with a greater problem size and it eventually levels off.
     This problem of calculating pi has decent scalability, but great care should be taken as to avoid a race condition, which may involve some overhead that could harm efficiency and scalability.